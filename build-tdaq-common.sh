#!/bin/bash
# 
# Build a subset of TDAQ Common with the ARM cross-compiler
# Run 'kinit -f <username>@CERN.CH first
#
mkdir -p /build/tdaq-common/build 
cd /build/tdaq-common
if [ ! -z "${CI_JOB_TOKEN}" ]; then 
    git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/atlas-tdaq-software/tdaq-common-cmake.git tdaq-common-99-00-00
else 
    git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-software/tdaq-common-cmake.git tdaq-common-99-00-00
fi
cd tdaq-common-99-00-00
git submodule update -i cmake_tdaq ers eformat EventStorage compression
# hack
(cd cmake_tdaq; git checkout arm)
cd ../build
export CMAKE_PREFIX_PATH=/build/tdaq-common/tdaq-common-99-00-00/cmake_tdaq/cmake:/build/tdaq-common/tdaq-common-99-00-00/cmake_tdaq/cmake/modules
/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.14.2/Linux-x86_64/bin/cmake -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake -D LCG_VERSION_CONFIG=LCG_999devARM/Tue  ../tdaq-common-99-00-00
make -j $(nproc) -k
make install/fast


https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/atlas-tdaq-software
