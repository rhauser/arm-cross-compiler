
Cross-compiler for ARM in a container
======================================

Building the image
------------------

```bash
% docker build -t arm-cross-compiler .
```

Running the image
-----------------

If you have build the container yourself:

```bash
% docker run -it -v /cvmfs:/cvmfs:ro,shared arm-cross-compiler bash
```

If you want to use the centrally built container:

```bash
% docker run -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/rhauser/arm-cross-compiler bash
```

See the /bin/build-tdaq[-common].sh scripts for an example to
use the cross-compiler with a toolchain file (see below).

Building a subset of the ATLAS TDAQ Common and TDAQ Release
------------------------------------------------------------

```bash
# kinit -f <username>@CERN.CH
build.sh
```

Or, for testing, project by project:

```bash
# kinit -f <username>@CERN.CH
# /bin/build-tdaq-common.sh
# /bin/build-tdaq.sh
```

You can pass the variables KRB5_USER and KRB5_PASSWORD into the container, then the
build.sh script will automatically initialize the ticket. This can e.g. be done via
secret environment variables in gitlab. 

Retrieving the tar.gz files
------------------------------

Simply mount the directory where you want the tar.gz files to appear
as /output into the container:

```bash
docker run -it -v /cvmfs:/cvmfs:ro,shared -v /work/tar:/output arm-cross-compiler bash
```

