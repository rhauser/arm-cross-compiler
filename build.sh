if [ ! -z "${KRB5_USER}" ]; then
    echo "${KRB5_PASSWORD}" | kinit -f "${KRB5_USER}"
fi
build-tdaq-common.sh
build-tdaq.sh
mkdir -p /output
cd /build/tdaq-common/tdaq-common-99-00-00
tar zcf /output/tdaq-common-99-00-00.tar.gz installed
cd /build/tdaq/tdaq-99-00-00
tar zcf /output/tdaq-99-00-00.tar.gz installed
