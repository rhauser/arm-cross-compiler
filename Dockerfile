FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7
RUN mkdir -p /arm/sysroot /arm/compiler
RUN cd /arm/compiler && curl -s -L https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz | tar Jxf - 
RUN yum install -y dnf java-1.8.0-openjdk-devel && yum clean all
RUN dnf -y --disablerepo="*" --installroot /arm/sysroot --releasever 7 --forcearch aarch64 --repofrompath init,http://mirror.centos.org/altarch/7/os/aarch64 --nogpgcheck install filesystem || true && dnf -y --disablerepo="*" --installroot /arm/sysroot --releasever 7 --forcearch aarch64 --repofrompath init,http://mirror.centos.org/altarch/7/os/aarch64 --nogpgcheck --setopt=tsflags=noscripts group install 'Minimal Install' || true && dnf -y  --nopgpcheck --installroot --setopt=tsflags=noscripts /arm/sysroot update || true && dnf -y --installroot /arm/sysroot clean all

# Add more RPMs to cross-compile sysroot as needed
RUN dnf -y --nogpgcheck --setopt=tsflags=noscripts --installroot /arm/sysroot --releasever 7 --forcearch aarch64 install motif-devel libXpm-devel libuuid-devel pam-devel libcurl-devel openldap-devel glibc-devel java-1.8.0-openjdk-devel || true && dnf -y --installroot /arm/sysroot clean all

ADD aarch64-toolchain.cmake /arm/aarch64-toolchain.cmake
ADD tdaq_user_postfix.cmake /arm/tdaq_user_postfix.cmake
RUN mkdir -p /cvmfs && ln -s /cvmfs /arm/sysroot/cvmfs
RUN mkdir -p /arm/sysroot/arm && ln -s /arm/compiler /arm/sysroot/arm
ADD build-tdaq-common.sh /bin
ADD build-tdaq.sh /bin
ADD build.sh /bin
