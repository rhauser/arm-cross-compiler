#!/bin/bash
# 
# Build a subset of TDAQ Common with the ARM cross-compiler
#
mkdir -p /build/tdaq/build
cd /build/tdaq
if [ ! -z "${CI_JOB_TOKEN}" ]; then
    git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake.git tdaq-99-00-00
else
    git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-software/tdaq-cmake.git tdaq-99-00-00
fi
cd tdaq-99-00-00
git submodule update -i AccessManager Jers cmdl config dal DFConfiguration dvs emon ExpertSystemInterface ipc is mts oks oks_utils omni owl ProcessManager RCInfo rdb rdbconfig ResourceManager RODBusyModule RunControl system TM tmgr xmext
# hack
(cd omni; git checkout arm)
cp /arm/tdaq_user_postfix.cmake .
cd ../build
export CMAKE_PREFIX_PATH=/build/tdaq-common/tdaq-common-99-00-00/cmake_tdaq/cmake:/build
/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.14.2/Linux-x86_64/bin/cmake -DJAVA_JVM_LIBRARY=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.232.b09-0.el7_7.x86_64/jre/lib/amd64/server/libjvm.so -DJAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.232.b09-0.el7_7.x86_64  -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake  ../tdaq-99-00-00
make -j $(nproc) -k
make install/fast
